
var list = [
	{'brand':'Brand', 'color':'#000', 'size':'56', 'price':'200'},
	{'brand':'Lacoste', 'color':'#fcc', 'size':'65', 'price':'300'},
	{'brand':'Prada', 'color':'#fab', 'size':'42', 'price':'400'},
	{'brand':'Abibas', 'color':'#00f', 'size':'large', 'price':'150'},
	{'brand':'Fuse', 'color':'#95c', 'size':'XL', 'price':'99'},
	{'brand':'Brand', 'color':'#000', 'size':'56', 'price':'200'},
	{'brand':'Lacoste', 'color':'#fcc', 'size':'65', 'price':'300'},
	{'brand':'Prada', 'color':'#fab', 'size':'42', 'price':'400'},
	{'brand':'Abibas', 'color':'#00f', 'size':'large', 'price':'150'},
	{'brand':'Fuse', 'color':'#95c', 'size':'XL', 'price':'99'},
	{'brand':'Brand', 'color':'#000', 'size':'56', 'price':'200'},
	{'brand':'Lacoste', 'color':'#fcc', 'size':'65', 'price':'300'},
	{'brand':'Prada', 'color':'#fab', 'size':'42', 'price':'400'},
	{'brand':'Abibas', 'color':'#00f', 'size':'large', 'price':'150'},
	{'brand':'Fuse', 'color':'#95c', 'size':'XL', 'price':'99'}
];

function openItem(args) {
	router.push('item',{});
};

function goBack() {
	router.goBack();
};

module.exports = {
	'list': list,
	'openItem': openItem,
	'goBack': goBack
};
